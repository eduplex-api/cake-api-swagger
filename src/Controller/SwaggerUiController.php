<?php

declare(strict_types = 1);

namespace Swagger\Controller;

use App\Controller\ApiController;
use Cake\Core\Configure;
use SplFileObject;

class SwaggerUiController extends ApiController
{
    public const JSON_PATH = 'json';

    public function isPublicController(): bool
    {
        return true;
    }

    protected function getList()
    {
        if (substr($this->getRequest()->getPath(), -1) !== '/') {
            return $this->redirect($this->getRequest()->getPath() . '/');
        }
        $jsonUrl = $this->getRequest()->getQuery('url');
        $default = Configure::read('SwaggerPlugin.defaultJson');
        if (!$jsonUrl && $default) {
            return $this->redirect($this->getRequest()->getPath() . '?url=' . $default);
        }
        if (!$jsonUrl) {
            $jsonUrl = $this->getRequest()->getPath() . self::JSON_PATH;
        }
        $this->response = $this->response->withStringBody($this->_getIndexHtml($jsonUrl));
        $this->return = $this->response;
    }

    protected function getData($id)
    {
        $string = $this->_readSwaggerFile($id);
        $extension = explode('.', $id)[1];
        $this->response = $this->response->withType($extension);

        $this->response = $this->response->withStringBody($string);
        $this->return = $this->response;
    }

    private function _readSwaggerFile(string $fileName): string
    {
        $filePath = ROOT . DS . 'vendor' . DS . 'swagger-api' . DS . 'swagger-ui' . DS . 'dist' . DS;
        $file = new SplFileObject($filePath . $fileName);

        $string = '';
        while (!$file->eof()) {
            $string .= $file->fgets();
        }
        return $string;
    }

    private function _getIndexHtml(string $jsonUrl = null): string
    {
        if ($jsonUrl) {
            $configScript = '<script> ' . $this->_getConfigScript($jsonUrl) . ' </script>';
        } else {
            $configScript = '<script src="./swagger-initializer.js" charset="UTF-8"> </script>';
        }
        $title = 'OpenAPI docs';
        $favicon = './favicon-32x32.png';
        $style = '.topbar {display:none;}';
        return '<!DOCTYPE html>
            <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>'.$title.'</title>
                <link rel="stylesheet" type="text/css" href="./swagger-ui.css" />
                <link rel="stylesheet" type="text/css" href="index.css" />
                <link rel="icon" type="image/png" href="'.$favicon.'" sizes="32x32" />
                <style>'.$style.'</style>
              </head>

              <body>
                <div id="swagger-ui"></div>
                <script src="./swagger-ui-bundle.js" charset="UTF-8"> </script>
                <script src="./swagger-ui-standalone-preset.js" charset="UTF-8"> </script>
                ' . $configScript . '
              </body>
            </html>';
    }

    private function _getConfigScript(string $url): string
    {
        return 'window.onload = function() {
                  window.ui = SwaggerUIBundle({
                    url: "'.$url.'",
                    dom_id: "#swagger-ui",
                    deepLinking: true,
                    presets: [
                        SwaggerUIBundle.presets.apis,
                        SwaggerUIStandalonePreset
                    ],
                    plugins: [
                        SwaggerUIBundle.plugins.DownloadUrl
                    ],
                    layout: "StandaloneLayout"
                  });
                };';
    }
}
