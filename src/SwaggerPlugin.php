<?php

declare(strict_types = 1);

namespace Swagger;

use Cake\Routing\RouteBuilder;
use RestApi\Lib\RestPlugin;

class SwaggerPlugin extends RestPlugin
{

    protected function routeConnectors(RouteBuilder $builder): void
    {
        $builder->connect('/swagger/ui/*', \Swagger\Controller\SwaggerUiController::route());
    }
}
