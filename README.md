# Cake-API-Profile

Simple endpoint to display [swagger-ui](https://github.com/swagger-api/swagger-ui)

## Works with
CakePHP Plugin to run on top of [cake-rest-api](https://packagist.org/packages/freefri/cake-rest-api).

## License
The source code for the site is licensed under the [**MIT license**](https://gitlab.com/eduplex-api), which you can find in the [LICENSE](../LICENSE/) file.
