<?php

namespace Swagger\Test\TestCase\Controller;

use RestApi\TestSuite\ApiCommonErrorsTest;
use Swagger\SwaggerPlugin;

class SwaggerUiControllerTest extends ApiCommonErrorsTest
{
    protected $fixtures = [
    ];

    protected function _getEndpoint(): string
    {
        return SwaggerPlugin::getRoutePath() . '/swagger/ui/';
    }

    public function testGetList()
    {
        $this->get($this->_getEndpoint() . '/?url=path.json');

        $body = $this->_getBodyAsString();
        $expected = '<!DOCTYPE html>
            <html lang="en">
              <head>
                <meta charset="UTF-8">
                <title>OpenAPI docs</title>';
        $this->assertStringStartsWith($expected, $body);
    }

    public function testGetData()
    {
        $this->get($this->_getEndpoint() . 'oauth2-redirect.html');

        $body = $this->_getBodyAsString();
        $expected = '<!doctype html>
<html lang="en-US">
<head>
    <title>Swagger UI: OAuth2 Redirect</title>';
        $this->assertStringStartsWith($expected, $body);
    }
}
